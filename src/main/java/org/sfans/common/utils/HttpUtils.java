package org.sfans.common.utils;

import javax.servlet.http.HttpServletRequest;

public class HttpUtils {

	/**
	 * url basePath
	 * 
	 * @param request
	 * @return
	 */
	public static String getBasePath(HttpServletRequest request) {
		StringBuilder basePath = new StringBuilder();
		basePath.append(request.getScheme())
				 .append("://")
				 .append(request.getServerName())
				 .append(":")
				 .append(request.getServerPort())
				 .append(request.getContextPath())
				 .append("/").toString();
		return basePath.toString();
	}
}
