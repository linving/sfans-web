package org.sfans.website.service;

import java.util.Optional;

import org.sfans.domain.Redirect;
import org.sfans.domain.Website;

public interface RedirectManager {

	Optional<Redirect> findRedirect(Website website, String url);
}
