package org.sfans.domain;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Entity
public class Post {
	@Id
	@GeneratedValue
	private long id;

	@Column(length = 100)
	private String title;
	private String description;
	
	@Column(length = 50)
	private String slug;
	
	@ManyToOne(optional = false)
	private Website website;
	
	@ElementCollection
	private final Set<String> labels = new TreeSet<>();
	
	@Version
	private long version;

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(final String slug) {
		this.slug = slug;
	}

	public Website getWebsite() {
		return website;
	}

	public void setWebsite(final Website website) {
		this.website = website;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(final long version) {
		this.version = version;
	}

	public Set<String> getLabels() {
		return labels;
	}

	@Override
	public String toString() {
		ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.NO_FIELD_NAMES_STYLE);
		return builder.append("id", id).append("description", description).append("slug", slug)
				.append("labels", labels).toString();
	}

}
