package org.sfans.services;

import org.sfans.domain.Redirect;

public interface RedirectService
{
    Redirect findByUri(String uri);
}
